package com.mcc.retrofitinkotlin.basics

fun main(args: Array<String>){




    // variable deceleration in Kotlin
    variableDecelerationInKotlin()

    // calling an anonymousFunction;
    anonymousFunctionExample()

    // lambda Expression
    lambdaExpression()

    nullAbility()

}

fun anonymousFunctionExample(){
    val num = fun(x: Int, y: Int): Int = x + y
    println(num(2,3))
}


fun variableDecelerationInKotlin(){
    val a: Int = 1  // immediate assignment
    val b = 2   // 'Int' type is inferred
    val c: Int  // Type required when no initializer is provided
    c = 3       // deferred assignment

    val result = a+b+c
    println("Result = $result")
}

fun lambdaExpression(){
    // Basic Lambda expression
    val sum = { x: Int, y: Int -> x + y }

    val printSomething = {

    }

    // Passing a Lambda as parameter
    val isEven = {x: Int -> x%2==0}
    println(isEven(sum(2,21)))
}

fun nullAbility(){
    val name: String? = null
    println(name?.length)
}