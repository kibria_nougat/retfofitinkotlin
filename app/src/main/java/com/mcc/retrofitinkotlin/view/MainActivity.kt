package com.mcc.retrofitinkotlin.view

import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import com.mcc.retrofitinkotlin.R
import com.mcc.retrofitinkotlin.interfaces.MovieApiCallListener
import com.mcc.retrofitinkotlin.model.data.Movie
import com.mcc.retrofitinkotlin.presenter.MainActivityPresenter
import com.mcc.retrofitinkotlin.view.adapter.MyMovieAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MovieApiCallListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
    }

    override fun onResume() {
        super.onResume()
        invokeMovieApi()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun invokeMovieApi(){
         val mainActivityPresenter = MainActivityPresenter(this)
         mainActivityPresenter.getMovieList()
    }

    override fun onSuccess(arrayList: ArrayList<Movie>?) {
        Toast.makeText(this, "Data fetching successful!", Toast.LENGTH_SHORT).show()

        rv_movies.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        rv_movies.adapter = MyMovieAdapter(applicationContext, arrayList)
    }

    override fun onFailure() {
        progress_bar?.visibility = View.INVISIBLE
        Toast.makeText(this, "Data fetching failed!", Toast.LENGTH_SHORT).show()
    }

    override fun showProgressBar() {
        progress_bar?.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        progress_bar?.visibility = View.INVISIBLE
    }

}
