package com.mcc.retrofitinkotlin.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.mcc.retrofitinkotlin.R
import com.mcc.retrofitinkotlin.model.data.Movie
import kotlinx.android.synthetic.main.item_movie_info.view.*

class MyMovieAdapter(private val context: Context, var itemArrayList: ArrayList<Movie>?) :
    RecyclerView.Adapter<MyMovieAdapter.MyViewHolder>() {

    private val imageBaseUrl = "https://image.tmdb.org/t/p/w500"
    private val map = HashMap<Int, String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_movie_info, parent, false)
        createGenres()
        return MyViewHolder(view)
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var imageUrl = itemArrayList!![position].posterPath
        loadImageWithGlide("$imageBaseUrl$imageUrl", holder.ivMoviePoster)
        holder.tvMovieTitle.text = itemArrayList!![position].title
        holder.tvMovieRatingText.text = "Ratings: " + itemArrayList!![position].voteAverage
        holder.rbMovieRate.rating = itemArrayList!![position].voteAverage.toFloat() / 2
        holder.tvMovieGenre.text = "Genre : " + getMovieGenres(itemArrayList!![position].genreIds)
        holder.tvMovieReleaseDate.text = "Release Date: "+itemArrayList!![position].releaseDate
        holder.tvMovieOverView.text = "Overview: "+itemArrayList!![position].overview

        if (itemArrayList!![position].adult) {
            holder.tvMovieType.text = "Type: ADULT"
        } else {
            holder.tvMovieType.text = "Type: NOT ADULT"
        }
        if (position < itemArrayList!!.size - 1) {
            holder.border.visibility = View.VISIBLE
        } else {
            holder.border.visibility = View.INVISIBLE
        }
    }


    override fun getItemCount(): Int {
        return itemArrayList!!.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val ivMoviePoster: ImageView = itemView.iv_movie_poster
        val tvMovieTitle: TextView = itemView.tv_movie_title
        val tvMovieRatingText: TextView = itemView.tv_movie_rating_text
        val rbMovieRate: RatingBar = itemView.rb_movie_rate
        val tvMovieGenre: TextView = itemView.tv_movie_genre
        val tvMovieType: TextView = itemView.tv_movie_type
        val tvMovieReleaseDate: TextView = itemView.tv_movie_release_date
        val tvMovieOverView: TextView = itemView.tv_movie_overview
        val border: View = itemView.border
    }

    private fun loadImageWithGlide(imageUrl: String, ivMoviePoster: ImageView) {
        Glide.with(context)
            .load(imageUrl)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.ic_launcher_background)
                    .circleCrop()
                    .fitCenter()
                    .override(200, 200)
            )
            .into(ivMoviePoster)
    }

    private fun getMovieGenres(genres: List<Int>): String {
        var movieGenres = ""
        for (genre in genres) {
            Log.d("DataTesting", ""+genre)
            movieGenres = if(movieGenres.equals("", true)) map.getValue(genre) else movieGenres+", "+map.getValue(genre)
        }
        return movieGenres
    }

    private fun createGenres() {
        map[28] = "Action"
        map[12] = "Adventure"
        map[16] = "Animation"
        map[35] = "Comedy"
        map[80] = "Crime"
        map[99] = "Documentary"
        map[18] = "Drama"
        map[10751] = "Family"
        map[14] = "Fantasy"
        map[36] = "History"
        map[27] = "Horror"
        map[10402] = "Music"
        map[9648] = "Mystery"
        map[10749] = "Romance"
        map[878] = "Science Fiction"
        map[10770] = "TV Movie"
        map[53] = "Thriller"
        map[10752] = "War"
        map[37] = "Western"
    }
}