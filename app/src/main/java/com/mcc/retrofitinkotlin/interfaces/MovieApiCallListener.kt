package com.mcc.retrofitinkotlin.interfaces

import com.mcc.retrofitinkotlin.model.data.Movie

interface MovieApiCallListener {
    fun onSuccess(movieArrayList : ArrayList<Movie>?)
    fun onFailure()
    fun showProgressBar()
    fun hideProgressBar()
}