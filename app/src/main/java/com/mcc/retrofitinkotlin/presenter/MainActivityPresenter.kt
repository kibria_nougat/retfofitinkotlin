package com.mcc.retrofitinkotlin.presenter

import com.mcc.retrofitinkotlin.interfaces.MovieApiCallListener
import com.mcc.retrofitinkotlin.model.data.Movie
import com.mcc.retrofitinkotlin.model.networking.InvokeApi

class MainActivityPresenter(var movieApiCallListener: MovieApiCallListener?) {

    fun getMovieList() {
        movieApiCallListener?.showProgressBar()
        val apiKey = "9a4176b10acbfa14bf4740762a7a4ecb"
        val releaseYear = "2018"
        val sortedBy = "popularity.desc"
        InvokeApi.getMovieList(apiKey, releaseYear, sortedBy, object : InvokeApi.OnApiCallResponse {
            override fun onFinish(arrayList: ArrayList<Movie>?) {
                movieApiCallListener?.hideProgressBar()
                movieApiCallListener?.onSuccess(arrayList)
            }

            override fun onFailure() {
                movieApiCallListener?.hideProgressBar()
                movieApiCallListener?.onFailure()
            }
        })
    }

}