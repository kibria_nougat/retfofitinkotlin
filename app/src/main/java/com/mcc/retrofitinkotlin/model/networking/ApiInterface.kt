package com.mcc.retrofitinkotlin.model.networking

import com.mcc.retrofitinkotlin.model.data.MovieResults
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("3/discover/movie?")
    fun retrieveRepositories(@Query("api_key") apiKey: String,
                             @Query("primary_release_year") releaseYear: String,
                             @Query("sort_by") sortBy: String): Call<MovieResults>

}