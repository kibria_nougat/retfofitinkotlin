package com.mcc.retrofitinkotlin.model.networking

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiClient {

    companion object {

        private const val BASE_URL = "https://api.themoviedb.org/"
        val apiService: ApiInterface

        init {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL) //1
                .addConverterFactory(GsonConverterFactory.create()) //2
                .build()
            apiService = retrofit.create(ApiInterface::class.java) //3
        }
    }

}