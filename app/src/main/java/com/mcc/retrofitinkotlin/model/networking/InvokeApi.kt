package com.mcc.retrofitinkotlin.model.networking

import android.util.Log.d
import com.mcc.retrofitinkotlin.model.data.Movie
import com.mcc.retrofitinkotlin.model.data.MovieResults
import retrofit2.Call
import retrofit2.Response


class InvokeApi {

    interface OnApiCallResponse {
        fun onFinish(arrayList: ArrayList<Movie>?)
        fun onFailure()
    }

    companion object {
        fun getMovieList(apiKey: String, releaseYear: String, sortedBy: String, onApiCallResponse: OnApiCallResponse) {
            d("DataTesting", "$apiKey  $releaseYear  $sortedBy")
            ApiClient.apiService.retrieveRepositories(apiKey, releaseYear, sortedBy)
                .enqueue(object : retrofit2.Callback<MovieResults> {

                    override fun onFailure(call: Call<MovieResults>?, t: Throwable?) {
                        d("DataTesting", "onFailure")
                        onApiCallResponse.onFailure()
                    }

                    override fun onResponse(call: Call<MovieResults>?, response: Response<MovieResults>?) {
                        d("DataTesting", ""+response?.body()?.movies?.size)
                        onApiCallResponse.onFinish(response?.body()?.movies)
                    }
                })
        }
    }

}